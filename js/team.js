$(document).ready(function () {
  $("#team-sortby").change(function () {
    if ($(this).val() != '') {
      window.location.href = $(this).val();
    }
  });
  $("#team-filter-division").change(function () {
    if ($(this).val() != '') {
      window.location.href = $(this).val();
    }
  });
  $("#team-filter-conference").change(function () {
    if ($(this).val() != '') {
      window.location.href = $(this).val();
    }
  });

});
