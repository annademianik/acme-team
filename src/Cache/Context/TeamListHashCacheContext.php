<?php

namespace Drupal\acme_team\Cache\Context;

use Drupal\acme_team\Service\TeamListServiceInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;

/**
 * Defines the TeamListHashCacheContext service for hash caching.
 */
class TeamListHashCacheContext implements CacheContextInterface {

  /**
   * Hash list service.
   *
   * @var \Drupal\acme_team\Service\TeamListServiceInterface
   */
  private TeamListServiceInterface $teamListService;

  /**
   * Constructs a new TeamListServiceInterface class.
   */
  public function __construct(TeamListServiceInterface $teamListService) {
    $this->teamListService = $teamListService;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Team List Hash');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->teamListService->getHash();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
