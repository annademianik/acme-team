<?php

namespace Drupal\acme_team\Plugin\Block;

use Drupal\acme_team\Service\TeamListServiceInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockManager;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This block is to group all the blocks in one for the footer.
 *
 * @Block(
 *   id = "team",
 *   admin_label = @Translation("Team Block"),
 *   category = @Translation("ACME")
 * )
 */
class TeamBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The block manager service.
   *
   * @var \Drupal\Core\Block\BlockManager
   */
  private BlockManager $blockManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $configFactory;

  /**
   * The team list service.
   *
   * @var \Drupal\acme_team\Service\TeamListServiceInterface
   */
  private TeamListServiceInterface $teamListManager;

  /**
   * The constructor of the block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Block\BlockManager $blockManager
   *   The block manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\acme_team\Service\TeamListServiceInterface $teamListManager
   *   The language_manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BlockManager $blockManager, ConfigFactoryInterface $configFactory, TeamListServiceInterface $teamListManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->blockManager = $blockManager;
    $this->configFactory = $configFactory;
    $this->teamListManager = $teamListManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.block'),
      $container->get('config.factory'),
      $container->get('acme_team.team_list_service')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // @todo Add some settings for form.
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    $data = $this->teamListManager->getData();
    return [
      '#theme' => 'acme_team_list',
      '#sorting_options' => $data['sorting_options'],
      '#team_members' => $data['team_members'],
      '#columns' => $data['columns'],
      '#result_count' => $data['result_count'],
      '#filters' => $data['filters'],
      '#clear_filters' => $data['clear_filters'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'url.query_args',
      'team_list_hash',
    ]);
  }

}
