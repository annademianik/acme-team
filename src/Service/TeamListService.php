<?php

namespace Drupal\acme_team\Service;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * PIM Service class for getting data from external PIM database.
 */
class TeamListService implements TeamListServiceInterface {

  /**
   * Full data from API.
   *
   * @var array
   */
  protected $data;

  /**
   * Team members array.
   *
   * @var array
   */
  protected $team;

  /**
   * Applied filters by label.
   *
   * @var array
   */
  protected $activeFiltersByLabel;

  /**
   * Table columns.
   *
   * @var array
   */
  protected $columns;

  /**
   * Default sorting parameter.
   */
  protected const DEFAULT_SORTING_OPTION = 'id_asc';

  /**
   * Allowed fields sorted by.
   */
  protected const ALLOWED_SORT_FIELDS = ['id', 'name'];

  /**
   * Allowed sorting order.
   */
  protected const ALLOWED_SORT_ARROWS = ['desc', 'asc'];

  /**
   * Fields for filtering.
   */
  protected const ALLOWED_FILTERS = ['division', 'conference'];

  /**
   * CredentialForm constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   *   A Guzzle client object.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   A request stack symfony instance.
   */
  public function __construct(protected Client $httpClient, protected RequestStack $requestStack) {
    $this->getApiData();
  }

  /**
   * Returns API URL.
   */
  protected function getApiUrl(): string {
    // Allow to override the host with an env variable.
    // @todo Remove api url from class.
    // @todo secure api key.
    return getenv('ACME_TEAM_LIST_URL') ?: 'http://delivery.chalk247.com/team_list/NFL.JSON?api_key=74db8efa2a6db279393b433d97c2bc843f8e32b0';
  }

  /**
   * {@inheritDoc}
   */
  public function getData(): array {
    if (!isset($this->data)) {
      return [];
    }
    $result = [
      'filters' => $this->getFilterOptions(),
      'sorting_options' => $this->getSortingOptions(),
      'team_members' => $this->processTeamMembers(),
      'columns' => $this->getColumns(),
      'result_count' => $this->getTeamMembersCount(),
      'clear_filters' => $this->getClearFilters(),
    ];

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function getApiData(): array {
    if (isset($this->data)) {
      return $this->data;
    }

    $uri = $this->getApiUrl();
    try {
      $request = $this->httpClient->get($uri, []);

      if ($request->getStatusCode() == '200') {
        $result = json_decode($request->getBody(), TRUE);
        $this->data = $result;
        $this->team = $result['results']["data"]["team"];
        $this->columns = $result['results']['columns'];
        return $result;
      }
    }
    catch (\Exception $exception) {
      // @todo log message;
    }

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getHash(): string {
    if ($this->data) {
      return $this->data['hash'];
    }
    return '0';
  }

  /**
   * {@inheritDoc}
   */
  public function getTeamMembersCount(): int {
    return count($this->team);
  }

  /**
   * Returns url query parameters.
   *
   * @return array
   *   Query params.
   */
  protected function getFilterParameters() {
    return $this->requestStack->getCurrentRequest()->query->all();
  }

  /**
   * Return some parameter from query.
   *
   * @param string $parameter
   *   Indeficator.
   */
  protected function getFilterParameter($parameter) {
    return $this->getFilterParameters()[$parameter] ?? FALSE;
  }

  /**
   * Returns columns.
   */
  protected function getColumns() {
    return $this->columns;
  }

  /**
   * Returns team.
   */
  protected function getTeamMembers() {
    return $this->team;
  }

  /**
   * Preprocess filtering and soring for team.
   */
  protected function processTeamMembers() {
    $this->processFiltering($this->team);
    $this->processSorting($this->team);
    return $this->team;
  }

  /**
   * Sorting team depend on choosed sort option.
   */
  protected function processSorting(&$team) {
    $choosed_sort = $this->getChoosedSorting();
    $sort_options = explode('_', $choosed_sort);

    // Just for better understanding:
    // $field could be: id, name.
    // $arrow could be: asc, desc.
    $field = $sort_options['0'];
    $arrow = $sort_options['1'];

    // @todo change method to get flag.
    $sort_by_flag = ["asc" => SORT_ASC, 'desc' => SORT_DESC];

    $field_value = array_column($team, $field);
    array_multisort($field_value, $sort_by_flag[$arrow], $team);
  }

  /**
   * Gets sorting option.
   */
  public function getChoosedSorting(): string {
    // Get sorting option from url params.
    $sort_by = $this->getFilterParameter('sort');
    if ($sort_by) {
      $sort_options = explode('_', $sort_by);

      // Ignore sorting from url if field is wrong.
      if (!in_array($sort_options['0'], self::ALLOWED_SORT_FIELDS)) {
        $sort_by = FALSE;
      }
      // Ignore sorting from url if arrow is wrong.
      if (!in_array($sort_options['1'], self::ALLOWED_SORT_ARROWS)) {
        $sort_by = FALSE;
      }
    }
    return $sort_by ?: self::DEFAULT_SORTING_OPTION;
  }

  /**
   * Get sorting options for selector.
   */
  protected function getSortingOptions() {
    // Get active sort option.
    $sort_by = $this->getChoosedSorting();

    // Generate current URL with current query params.
    $url = $this->getCurrentUrl();
    $query = $this->getFilterParameters();

    $columns_from_api = $this->getColumns();
    $sort_options = [];
    foreach (self::ALLOWED_SORT_FIELDS as $sort_by_field) {

      // Lets skip this field if it is not exist in API.
      if (!isset($columns_from_api[$sort_by_field])) {
        continue;
      }

      foreach (self::ALLOWED_SORT_ARROWS as $sort_by_arrow) {
        $sort_option = $sort_by_field . '_' . $sort_by_arrow;

        // Add sort options to existed query.
        $query["sort"] = $sort_option;
        $url->setOptions(['query' => $query]);
        $sort_options[] = [
          "label" => $columns_from_api[$sort_by_field] . ' ' . $sort_by_arrow,
          'checked' => $sort_option == $sort_by,
          'value' => $url->toString(),
        ];
      }
    }
    return $sort_options;

  }

  /**
   * Filter team member.
   *
   * @param array $team
   *   Team list.
   */
  protected function processFiltering(array &$team) {
    if (!isset($this->activeFiltersByLabel)) {
      $this->getFilterOptions();
    }
    foreach ($team as $key => $team_member) {

      foreach ($this->activeFiltersByLabel as $filter_name => $active_fields) {
        if (!isset($active_fields[$team_member[$filter_name]])) {
          unset($team[$key]);
        }
      }
    }
  }

  /**
   * Gets select lists of filters with options.
   *
   * @return array
   *   Prepared select lists.
   */
  protected function getFilterOptions(): array {
    $url_params = $this->getFilterParameters();

    // Prepare current url with current params.
    $url = $this->getCurrentUrl();
    $url->setOptions(['query' => $url_params]);
    $filters_info = [];
    $team = $this->getTeamMembers();
    $columns_from_api = $this->getColumns();
    $checked_filters_by_label = [];

    foreach ($team as $team_member) {
      foreach (self::ALLOWED_FILTERS as $filter) {

        // Lets skip this field if it is not exist in API.
        if (!isset($columns_from_api[$filter])) {
          continue;
        }

        $filter_value = $this->getMachineName($team_member[$filter]);
        $filter_checked = FALSE;

        // Variable to generate option url.
        $option_filter_query = $url_params;
        // If Filter exist in url query.
        if (isset($url_params[$filter])) {
          $query_params = explode(',', $url_params[$filter]);

          // If filter has current option value.
          // Save option label to $checked_filters_by_label.
          if (in_array($filter_value, $query_params)) {
            $filter_checked = TRUE;
            $checked_filters_by_label[$filter][$team_member[$filter]] = 0;
          }
          else {
            $filter_url_options = $query_params;
            $filter_url_options[] = $filter_value;
            $option_filter_query[$filter] = implode(',', $filter_url_options);
            $url->setOptions(['query' => $option_filter_query]);
          }
        }
        else {
          $option_filter_query[$filter] = $filter_value;
          $url->setOptions(['query' => $option_filter_query]);
        }

        $filters_info[$filter]['options'][$filter_value] = [
          "label" => $team_member[$filter],
          'checked' => $filter_checked,
          'value' => $url->toString(),
        ];
        $filters_info[$filter]['id'] = $filter;
      }
    }
    $this->activeFiltersByLabel = $checked_filters_by_label;
    return $filters_info;
  }

  /**
   * Generate machine name from string.
   *
   * @param string $string
   *   Machine name.
   */
  protected function getMachineName($string): string {
    // We don't have machine name of options,
    // so lets generate machine name for filter values.
    // @todo change this method or put method to dependency injection.
    $transliterated = \Drupal::transliteration()
      ->transliterate($string, LanguageInterface::LANGCODE_DEFAULT, '_');
    $transliterated = mb_strtolower($transliterated);

    $transliterated = preg_replace('@[^a-z0-9_.]+@', '_', $transliterated);

    return $transliterated;
  }

  /**
   * {@inheritDoc}
   */
  public function getClearFilters(): array {
    $url_params = $this->getFilterParameters();
    $url = $this->getCurrentUrl();

    // Lets keep sorting after filter resetting.
    if (isset($url_params['sort'])) {
      $url->setOptions(['query' => ['sort' => $url_params['sort']]]);
      unset($url_params['sort']);
    }
    return [
      'show' => !empty($url_params),
      'url' => $url->toString(),
    ];
  }

  /**
   * Get current url.
   *
   * @return \Drupal\Core\Url
   *   Current url.
   */
  protected function getCurrentUrl(): Url {
    $url = Url::fromRoute('<current>');
    return $url;
  }

}
