<?php

namespace Drupal\acme_team\Service;

/**
 * PIM Service class for getting data from external PIM database.
 */
interface TeamListServiceInterface {

  /**
   * Prepares variables for template.
   *
   * @return array
   *   Array with prepared data
   */
  public function getData(): array;

  /**
   * Get data from API endpoint..
   *
   * @return array
   *   An array of all data
   */
  public function getApiData(): array;

  /**
   * Provides hash string from request.
   *
   * @return string
   *   Hash.
   */
  public function getHash(): string;

  /**
   * Return count of team results.
   *
   * @return int
   *   Count result.
   */
  public function getTeamMembersCount(): int;

  /**
   * Genarate url without query params except sort.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   URL with cleared filter params.
   */
  public function getClearFilters(): array;

  /**
   * Gets sorting option.
   */
  public function getChoosedSorting(): string;

}
