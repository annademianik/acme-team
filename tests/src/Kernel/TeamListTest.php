<?php

namespace Drupal\Tests\acme_team\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Test class for the service acme_team.team_list_service.
 *
 * @group acme_team
 */
class TeamListTest extends KernelTestBase {

  /**
   * The TeamListService service.
   *
   * @var \Drupal\acme_team\Service\TeamListServiceInterface
   */
  protected $teamListManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'acme_team',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->teamListManager = $this->container->get('acme_team.team_list_service');
  }

  /**
   * Test if paths needs redirections.
   */
  public function testTeamDataFormat() {
    $data = $this->teamListManager->getData();
    $this->assertIsArray($data);

    // @todo fix current url redirection.
    $test_path = '/test?sort=id_desc';
    $response = new RedirectResponse($test_path);
    $response->send();
    $data = $this->teamListManager->getClearFilters();
    $this->assertIsArray($data);
    // $this->assertFalse($data['show']);
    $test_path = '/test?sort=id_desc&devision=west';
    $response = new RedirectResponse($test_path);
    $response->send();
    $data = $this->teamListManager->getClearFilters();
    $this->assertIsArray($data);
    // $this->assertTrue($data['show']);
  }

  /**
   * Test sorting.
   */
  public function testTeamSorting() {
    // Allowed sorting value.
    $test_path = '/2test?sort=id_desc';
    $response = new RedirectResponse($test_path);
    $response->send();
    $data = $this->teamListManager->getChoosedSorting();
    // $this->assertEquals('id_desc', urldecode($data));
    // Wrong sorting parameter.
    $test_path = '/test?sort=bla_asc';

    $response = new RedirectResponse($test_path);
    $response->send();
    $data = $this->teamListManager->getChoosedSorting();
    // Expected default sorting value;.
    // $this->assertEquals("id_asc", urldecode($data));
  }

}
